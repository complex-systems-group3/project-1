import os
import random

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm


def neighbourhood_frequency_counter(dim):
	count = [0] * dim
	for i in range(0, np.power(2, dim)):
		binary = decimal_to_binary(i, dim)
		print(binary)
		neigh = np.array([binary[-1], binary[0], binary[1]])
		count[-binary_to_decimal(neigh) - 1] = count[-binary_to_decimal(neigh) - 1] + 1
		for j in np.arange(1, dim - 1):
			neighbourhood = binary[j - 1:j + 2]
			print(neighbourhood)
			print(binary_to_decimal(neighbourhood))
			count[-binary_to_decimal(neighbourhood) - 1] = count[-binary_to_decimal(neighbourhood) - 1] + 1
		neigh = np.array([binary[j - 1], binary[j], binary[0]])
		count[-binary_to_decimal(neigh) - 1] = count[-binary_to_decimal(neigh) - 1] + 1
	return count


def distribution(x, div):
	'''
	Make a histogram from x with bin size div.
	'''
	histogram = [0] * div
	dx = (max(x) + 0.001 - min(x)) * 1.0 / div

	for j in np.arange(len(x)):
		pos = int((x[j] - min(x)) / dx)
		histogram[pos] = histogram[pos] + 1
	histogram = np.dot(histogram, 1 / np.sum(histogram))
	return np.linspace(min(x), max(x), div), histogram


def decimal_to_binary(x, dim):
	# 0<=x<=255
	x = x % (np.power(2, dim))
	bin = np.zeros((dim))
	i = 0
	while (x > 0):
		bin[-i - 1] = x % 2
		x = x // 2
		i = i + 1
	return bin


def balanced_binary_vector(dim):
	'''
	Create a vector of length 'dim'.
	Balanced in zeroes and ones.
	'''
	dim = dim + dim % 2
	x = [0] * dim
	n_ones = dim / 2
	n_zeros = dim / 2
	for i in np.arange(dim):
		go = 0
		while (go == 0):
			add = random.randint(0, 1)
			if (add == 1 and n_ones > 0):
				x[i] = 1
				n_ones = n_ones - 1
				go = 1
			if (add == 0 and n_zeros > 0):
				x[i] = 0
				n_zeros = n_zeros - 1
				go = 1
	return x


def get_density(x):
	return np.sum(x) / len(x)


def randIntFromDistribution(probs):
	x = np.random.uniform(0, 1)
	cont = probs[0]
	i = 0
	while (x >= cont):
		i = i + 1
		cont = cont + probs[i]
	return i


def binary_to_decimal(x):
	dec = 0
	for i in np.arange(len(x)):
		dec = dec + np.power(2, i) * x[-1 - i]
	return int(dec)


class CA(object):

	def __init__(self, initX=None, dim=250):
		'''
		initX: optional initial array
		dim: length
		self.trajectory: list of lists
		'''
		if initX == None:
			initX = balanced_binary_vector(dim)
		self.trajectory = []
		self.trajectory.append(initX)
		self.len = len(initX)

	def density(self):
		return get_density(self.trajectory[-1])

	def evolve(self, n_functions, probabilities, t_max=500, verbose=False):
		'''
		n_functions: array with different rules, given as decimal numbers
		probabilities: array with probability per rule

		Appends t_max sequences to self.trajectory,
		following the rules
		'''
		# Save parameters for later reuse
		self.last_run = {
			'functions': n_functions,
			'probabilities': probabilities,
			't_max': t_max
		}
		# Convert the functions to their binary definitions
		functions = []
		for i in np.arange(len(n_functions)):
			functions.append(decimal_to_binary(n_functions[i], 8))

		functions = np.array(functions)
		if verbose:
			print(f'Starting CA evolution with:\n   rule 1: {n_functions[0]}\n   rule 2: {n_functions[1]}\n   lambda: {probabilities[1]}')

		# normalize probabilities
		probabilities = np.dot(probabilities, np.sum(probabilities))

		for t in np.arange(t_max):
			if verbose:
				print(f'   time: {t}/{t_max}', end='\r')

			new_sequence = [0] * (self.len)

			# I select the function
			l = randIntFromDistribution(probabilities)
			function = functions[l]
			# print("I choose function ",i)
			# first boundary position
			neigh = np.array([self.trajectory[-1][-1], self.trajectory[-1][0], self.trajectory[-1][1]])
			new_sequence[0] = int(function[-binary_to_decimal(neigh) - 1])

			for i in range(1, self.len - 1):
				neigh = self.trajectory[-1][i - 1:i + 2]
				l = randIntFromDistribution(probabilities)
				function = functions[l]
				new_sequence[i] = int(function[-binary_to_decimal(neigh) - 1])
			# last boundary position
			l = randIntFromDistribution(probabilities)
			function = functions[l]
			neigh = np.array([self.trajectory[-1][-2], self.trajectory[-1][-1], self.trajectory[-1][0]])

			new_sequence[-1] = int(function[-binary_to_decimal(neigh) - 1])

			self.trajectory.append(new_sequence)

		if verbose:
			print(f"   density: {self.density()}")
			print()

	def show_trajectory(self):
		for i in np.arange(len(self.trajectory)):
			print()
			for j in np.arange(len(self.trajectory[i])):

				if (self.trajectory[i][j] == 1):
					print("\033[1;32;40m 1", end=' ')
				else:
					print("\033[1;32;47m 0", end=' ')

		print()

	def plot_trajectory(self):
		current_dir = os.path.dirname(os.path.realpath(__file__))
		output_dirname = "figures"
		output_dir = os.path.join(current_dir, output_dirname)
		os.makedirs(output_dir, exist_ok=True)

		rule_1 = self.last_run['functions'][0]
		rule_2 = self.last_run['functions'][1]
		λ = self.last_run['probabilities'][1]
		t_max = self.last_run['t_max']
		size = self.len

		extension = 'png'
		filename = f'rule{rule_1}_rule{rule_2}_lambda{λ:.2f}_n{size}_t{t_max}.{extension}'

		path = os.path.join(output_dir, filename)

		plt.imsave(path, self.trajectory, cmap=cm.gray)

	def density_distribution_properties(self, dim, n_functions, probabilities, t_max, samples):
		'''
		Gives standard deviation and the mean of a sample.
		'''
		densities = []
		for i in np.arange(samples):
			print("sample ", i, "/", samples)
			self.trajectory = []
			self.trajectory.append(balanced_binary_vector(dim))
			self.evolve(n_functions, probabilities, t_max)
			densities.append(self.density())
		return densities, np.mean(densities), np.std(densities)

	def densityVsLambda(self, dim, n_functions, probabilities, t_max, samples, lambdaMin, lambdaMax, div):
		# this method takes as input
		# dim dimension of CA array/trajectory line
		# n_functions array of function numbers
		# probabilities array of probabilities
		# t_max number of lines for each trajectory
		# samples number of times the trajectory is repeated to sample the stochastic variable "density"
		# lambdaMin .. div define the lambda interval where simulation is repeated
		lmbdas = np.linspace(lambdaMin, lambdaMax, div)
		mDensities = []
		sigmas = []
		for lmbda in lmbdas:
			mDensities.append(self.density_distribution_properties(
				dim, n_functions, probabilities, t_max, samples)[1])
			sigmas.append(self.density_distribution_properties(dim, n_functions,
															   probabilities, t_max, samples)[2])
			mDensities = np.array(mDensities)
			sigmas = np.array(sigmas)

		return lmbdas, mDensities, sigmas


def questions_part2_asynchronous_CA():
	# for part II, 2.1 Asynchronous CA
	n = 250
	t = 500
	rule_2 = 204

	for rule_1 in [6, 50, 178]:
		for alpha in [0.25, 0.50, 0.75]:
			Ca = CA(dim=n)
			Ca.evolve([rule_1, rule_2], [alpha, 1 - alpha], t_max=t, verbose=True)
			Ca.plot_trajectory()


def main():
	questions_part2_asynchronous_CA()


if __name__ == '__main__':
	main()
